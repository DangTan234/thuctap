import React, { Component } from 'react';
//
import GridItem from "components/Grid/GridItem.js";
import GridContainer from "components/Grid/GridContainer.js";
import Button from "components/CustomButtons/Button.js";
import Card from "components/Card/Card.js";
import CardHeader from "components/Card/CardHeader.js";
import CardBody from "components/Card/CardBody.js";
import CardFooter from "components/Card/CardFooter.js";
//
import ImageUploader from 'react-images-upload';
//
import axios from 'axios';
class Add extends Component {
    constructor(props) {
        super(props)
        this.state = {
            Item: '',
            Quantity: '',
            Type: '',
            Price: '',
            file: null
        };
        this.onhandlechange = this.onhandlechange.bind(this);
        this.onHandleSubmit = this.onHandleSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
    }
    onChange(e) {
        this.setState({file:e.target.files[0]});
    }
    onhandlechange(event) {
        var target = event.target;
        var name = target.name;
        var value = target.value;
        this.setState({ [name]: value });
    }
    onHandleSubmit(event) {
        event.preventDefault();
        console.log(this.state);
        axios({
            method: 'post',
            url: 'https://5ee03ae79ed06d001696dc32.mockapi.io/api/item',
            data: {
                Item: this.state.Item,
                Quantity: this.state.Quantity,
                Type: this.state.Type,
                Price: this.state.Price,
                pictures: this.state.picture,
            }
        });
    }
    render() {
        return (
            <div>
                <form on onSubmit={this.onHandleSubmit}>
                    <GridContainer>
                        <GridItem xs={12} sm={12} md={8}>
                            <Card>
                                <CardHeader color="primary">
                                    <h4 >Add Item</h4>
                                    <p >Complete This Form To Add An Item To API</p>
                                </CardHeader>
                                <CardBody>
                                    <GridContainer>
                                        <label>tên sản phẩm</label>

                                        <GridItem xs={12} sm={12} md={15}>

                                            <input
                                                labelText="Name"
                                                name="Item"
                                                className="form-control"
                                                onChange={this.onhandlechange}
                                            />
                                        </GridItem>
                                        
                                        <label>số lượng nhập</label>


                                        <GridItem xs={12} sm={12} md={15}>
                                            <input
                                                labelText="Quantity"
                                                name="Quantity"
                                                className="form-control"
                                                onChange={this.onhandlechange}
                                            />
                                        </GridItem>
                                        <label>loại Sản Phẩm</label>

                                        <GridItem xs={12} sm={12} md={15}>
                                            <input
                                                labelText="Type"
                                                name="Type"
                                                className="form-control"
                                                onChange={this.onhandlechange}
                                            />
                                        </GridItem>
                                        <label>Giá Sản Phẩm</label>

                                        <GridItem xs={12} sm={12} md={15}>
                                            <input
                                                labelText="Price"
                                                name="Price"
                                                className="form-control"
                                                onChange={this.onhandlechange}
                                            />
                                        </GridItem>
                                    </GridContainer>
                                </CardBody>
                                <CardFooter>
                                    <Button type="submit" color="primary" >Submit</Button>
                                    <Button type="reset" color="primary" >Reset</Button>
                                </CardFooter>
                            </Card>
                        </GridItem>
                        <GridItem xs={12} sm={12} md={4}>
                            <Card profile>

                                <ImageUploader
                                    withIcon={true}
                                    buttonText='Choose images'
                                    onChange={this.onDrop}
                                    imgExtension={['.jpg', '.gif', '.png', '.gif']}
                                    maxFileSize={40960000}
                                />
                            </Card>
                        </GridItem>
                    </GridContainer>
                </form>
            </div>
        );
    }
}

export default Add;